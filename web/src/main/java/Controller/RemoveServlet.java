package Controller;


import services.AddBookService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/RemoveServlet")
public class RemoveServlet extends HttpServlet {
    private AddBookService bookService;

    public RemoveServlet() {
        this.bookService = new AddBookService();
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
req.getRequestDispatcher("RemoveServlet").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String removeId = req.getParameter("choose");
        try{
        bookService.removeBookById(removeId);
        }
        catch (NullPointerException e){
            String bug = "Book with this id as been not found !";
            req.setAttribute("bookNotFound",bug);
            req.getRequestDispatcher("/HomeServlet").include(req,resp);
        }
        resp.sendRedirect("/HomeServlet");

    }
}
