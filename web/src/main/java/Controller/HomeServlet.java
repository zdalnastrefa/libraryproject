package Controller;


import dto.BookDto;
import services.BookDtoService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/HomeServlet")
public class HomeServlet extends HttpServlet {

    private BookDtoService bookDtoService;

    public HomeServlet() {
        this.bookDtoService = new BookDtoService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<BookDto> books = bookDtoService.allBooksDto();
        req.setAttribute("books", books);
        req.getRequestDispatcher("index.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String submit = req.getParameter("submit");
        String bookId = req.getParameter("choose");
        switch (submit) {
            case "add":
                resp.sendRedirect("/AddServlet");
                break;
            case "edit":
                resp.sendRedirect("/EditServlet?bookId=" + bookId);
                break;
            case "delete":
                req.getRequestDispatcher("/RemoveServlet").forward(req,resp);
                resp.sendRedirect("/RemoveServlet?bookId" + bookId);
                break;
            case "show":
                break;
            default:
                break;

        }
    }
}
