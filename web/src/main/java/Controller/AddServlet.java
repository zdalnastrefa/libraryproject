package Controller;

import models.Author;
import models.Book;
import services.AddBookService;
import services.AuthorService;
import services.BookDtoService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

@WebServlet("/AddServlet")
public class AddServlet extends HttpServlet {
    private BookDtoService bookDtoService;
    private AuthorService authorService;

    public AddServlet() {
        this.bookDtoService = new BookDtoService();
        this.authorService = new AuthorService();
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Book> specBooks = bookDtoService.findAll();
        req.setAttribute("specificBook",specBooks);
        List<Author> authors = authorService.listOfAuthors();
        req.setAttribute("authors",authors);
        req.getRequestDispatcher("addBook.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String title = req.getParameter("title");
        String releaseDate = req.getParameter("releaseDate");
        LocalDate date = LocalDate.parse(releaseDate);
        String summary = req.getParameter("summary");
        String author = req.getParameter("author");
        String category = req.getParameter("category");
        String isbn = req.getParameter("isbn");
        String pages = req.getParameter("pages");
        AddBookService addBookService = new AddBookService();
        addBookService.addNewBook(title,category,isbn,Integer.valueOf(pages),date,summary,author);
        resp.sendRedirect("bookAdded.jsp");
    }
}
