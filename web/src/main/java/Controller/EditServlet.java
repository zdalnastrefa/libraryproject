package Controller;

import dto.BookDto;
import models.Author;
import models.Book;
import repository.AuthorRepository;
import services.AddBookService;
import services.AuthorService;
import services.BookDtoService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

@WebServlet("/EditServlet")
public class EditServlet extends HttpServlet {
    private BookDtoService bookDtoService;
    private AuthorService authorService;
    private AddBookService addBookService;

    public EditServlet() {
        this.bookDtoService = new BookDtoService();
        this.authorService = new AuthorService();
        this.addBookService = new AddBookService();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String releaseDate = req.getParameter("releaseDate");
        Book newBook = addBookService.createBook(req.getParameter("title"), req.getParameter("category"), req.getParameter("isbn"),
                Integer.valueOf(req.getParameter("pages")), LocalDate.parse(releaseDate),
                req.getParameter("summary"), req.getParameter("author"));
        addBookService.editBook(newBook);
        resp.sendRedirect("/HomeServlet");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        BookDto foundedBook = bookDtoService.findOne(req.getParameter("bookId"));
        List<Author> authors = authorService.listOfAuthors();
        req.setAttribute("authors", authors);
        req.setAttribute("bookDto", foundedBook);
        req.getRequestDispatcher("edit.jsp").forward(req, resp);
    }
}
