<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Home Page</title>
    <link rel="stylesheet" href="/webjars/bootstrap/4.0.0-2/css/bootstrap.min.css">
</head>
<body>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">Home</li>
    </ol>
</nav>
<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">Title</th>
        <th scope="col">Author</th>
        <th scope="col">ISBN</th>
        <th scope="col">Category</th>
        <th scope="col">Date of release</th>
        <th scope="col">Borrowed by</th>
        <th scope="col">Choose</th>
    </tr>
    </thead>
    <tbody>
    <form action="/HomeServlet" method="post">
        <c:forEach items="${requestScope.books}" var="book">
            <tr>
                <th scope="row">${book.title}</th>
                <td>${book.authorName}</td>
                <td>${book.isbn}</td>
                <td>${book.category}</td>
                <td>${book.dateOfRelease}</td>
                <td>${book.borrower}</td>
                <td><input type="radio" name="choose" value="${book.bookId}" checked> </input> </td>
            </tr>
        </c:forEach>
        <div class="container">
            <button type="submit" name="submit" value="add" class="btn btn-outline-success">Add book</button>
            <button type="submit" name="submit" value="remove" class="btn btn-outline-danger">Remove Book</button>
            <button type="submit" name="submit" value="edit" class="btn btn-outline-info">Edit Book</button>
            <button type="submit" name="submit" value="show" class="btn btn-outline-secondary">Show details / Borrow</button>
        </div>
    </form>
    </tbody>
</table>


</body>
</html>
