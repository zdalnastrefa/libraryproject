<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="form-row">
        <div class="form-group col-md-6">
            <label for="title">Title</label>
            <input type="text" class="form-control" id="title" name="title" value="${requestScope.bookDto.title}" placeholder=">>Title<<">
        </div>
        <div class="form-group col-md-6">
            <label for="releaseDate">Date of Release</label>
            <input type="date" class="form-control" value="${requestScope.bookDto.releaseDate}" name="releaseDate" id="releaseDate">
        </div>
    </div>
    <div class="form-group">
        <label for="inputDesc">Description</label>
        <textarea rows="4"
                   type="text" class="form-control" name="summary" id="inputDesc" placeholder="Put description here">${requestScope.bookDto.description}</textarea>  </div>
    <div class="form-group">
        <label for="author">Author</label>
        <select id="author" name="author" class="form-control">
            <c:forEach items="${requestScope.authors}" var="author">
            <option value="${author.idAuthor}">${author.authorName} ${author.authorLastname}</option>
            </c:forEach>
        </select>
    </div>
    <div class="form-group col-md-2">
        <label for="inputState">Category</label>
        <select id="inputState" name="category" class="form-control">
            <option value="ADVENTURE" selected>Adventure</option>
            <option value="HORROR">Horror</option>
            <option value="ROMANCE">Romance</option>
            <option value="COMEDY">Comedy</option>
        </select>
        <label for="inputZip">ISBN</label>
        <input type="number" name="isbn" value="${requestScope.bookDto.isbn}" class="form-control" id="inputZip">
        <label for="pages">Pages</label>
        <input type="number" name="pages" class="form-control" value="${requestScope.bookDto.pages}" id="pages">
    </div>
    </div>
