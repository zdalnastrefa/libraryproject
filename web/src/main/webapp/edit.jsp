<%--
  Created by IntelliJ IDEA.
  User: wojta
  Date: 05.06.2018
  Time: 19:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit Book</title>
    <link rel="stylesheet" href="/webjars/bootstrap/4.0.0-2/css/bootstrap.min.css">

</head>
<body>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="http://localhost:7070/HomeServlet">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Edit book</li>
    </ol>
</nav>
<div class="container">
<form action="EditServlet" method="post">
<jsp:include page="WEB-INF/bookForm.jsp"/>
    <button type="submit" class="btn btn-primary">Edit</button>
</form>
</div>
<p><a href="/HomeServlet"><button type="button" class="btn btn-outline-danger">Cancel</button></a></p>

</body>
</html>
