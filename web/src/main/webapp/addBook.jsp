<%--
  Created by IntelliJ IDEA.
  User: wojta
  Date: 01.06.2018
  Time: 18:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="/webjars/bootstrap/4.0.0-2/css/bootstrap.min.css">

    <title>Add book !</title>
</head>
<body>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="http://localhost:7070/HomeServlet">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Add book</li>
    </ol>
</nav>
<div class="container">
    <form action="AddServlet" method="post" autocomplete="on">
    <jsp:include page="WEB-INF/bookForm.jsp"/>
        <button type="submit" class="btn btn-primary">Add</button>
    </form>
</div>
<div><br>
<p><a href="/HomeServlet"><button type="button" class="btn btn-outline-danger">Cancel</button></a></p>
</div>
</body>
</html>
