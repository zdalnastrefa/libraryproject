package services;

import Enums.CategoryEnum;
import dto.BookDto;
import models.Author;
import models.Book;
import repository.AuthorRepository;
import repository.BookRepository;

import java.sql.Date;
import java.time.LocalDate;


public class AddBookService {
    private BookRepository bookRepository;

    public AddBookService() {
        this.bookRepository = new BookRepository();
    }

    public void editBook(Book book){
        bookRepository.edit(book);
    }

    public void addNewBook(String title, String category, String isbn, int pages, LocalDate dateOfRelease, String summary, String authorId){
        Book newBook = new Book();
        newBook.setTitle(title);
        newBook.setCategory(CategoryEnum.valueOf(category));
        newBook.setIsbn(isbn);
        newBook.setPages(pages);
        newBook.setReleaseDate(dateOfRelease);
        newBook.setSummary(summary);
        AuthorRepository authorRepository = new AuthorRepository();
        Author authorFound = authorRepository.read(Long.valueOf(authorId));
        newBook.setAuthor(authorFound);
        bookRepository.create(newBook);

    }
    public void removeBookById(String id){
        Long longId = Long.valueOf(id);
        bookRepository.delete(bookRepository.read(longId));
    }
    public Book createBook(String title, String category, String isbn, int pages, LocalDate dateOfRelease, String summary, String authorId){
        Book newBook = new Book();
        newBook.setTitle(title);
        newBook.setCategory(CategoryEnum.valueOf(category));
        newBook.setIsbn(isbn);
        newBook.setPages(pages);
        newBook.setReleaseDate(dateOfRelease);
        newBook.setSummary(summary);
        AuthorRepository authorRepository = new AuthorRepository();
        Author authorFound = authorRepository.read(Long.valueOf(authorId));
        newBook.setAuthor(authorFound);
        return newBook;
    }
}
