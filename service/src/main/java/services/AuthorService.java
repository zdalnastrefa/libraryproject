package services;

import models.Author;
import repository.AuthorRepository;

import java.util.List;

public class AuthorService {
    public List<Author> listOfAuthors() {
        AuthorRepository authorRepository = new AuthorRepository();
        return authorRepository.findAll();
    }
}
