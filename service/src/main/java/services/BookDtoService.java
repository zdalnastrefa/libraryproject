package services;

import Enums.CategoryEnum;

import dto.BookDto;
import models.Author;
import models.Book;
import models.Borrow;
import models.Borrower;
import repository.AuthorRepository;
import repository.BookRepository;

import java.time.LocalDate;
import java.util.*;
public class BookDtoService {

    private BookRepository bookRepository;

    public BookDtoService() {
        this.bookRepository = new BookRepository();
    }




    public List<Book> findAll(){
        List<Book> all = bookRepository.findAll();
        return all;
    }

    public BookDto findOne(String id){
        Long idLong = Long.valueOf(id);
        Book read = bookRepository.read(idLong);
        String authorDisplay = read.getAuthor().getAuthorName() + " " + read.getAuthor().getAuthorLastname();
        BookDto bookDto = createBookDto(read.getTitle(), authorDisplay, read.getIsbn(), read.getCategory(), read.getReleaseDate(), findCurrentBorrower(read), read.getBookId(), read.getSummary(), read.getPages());
        return bookDto;
    }

    public List<BookDto> allBooksDto() {
        List<BookDto> dtoBookList = new ArrayList<BookDto>();
        BookRepository bookRepository = new BookRepository();
        List<Book> allBooks = bookRepository.findAll();
        for (Book b : allBooks) {
            String authorDisplay = b.getAuthor().getAuthorName() + " " + b.getAuthor().getAuthorLastname();
            BookDto bookDto = createBookDto(b.getTitle(), authorDisplay, b.getIsbn(), b.getCategory(), b.getReleaseDate(), findCurrentBorrower(b),b.getBookId(),b.getSummary(),b.getPages());
            dtoBookList.add(bookDto);
        }
        return dtoBookList;
    }

    private String findCurrentBorrower(Book book) {
        String borrowerName = null;
        StringBuilder sb = new StringBuilder();
        if (book.isBorrow()) {
            Optional<Borrower> borrower = book.getBorrows().stream()
                    .max(Comparator.comparing(Borrow::getIdBorrow))
                    .map(Borrow::getBorrowerId);
            if (borrower.isPresent()) {
                borrowerName = borrower.get().getBorrowerName();
                sb.append(borrowerName);
                borrowerName = borrower.get().getBorrowerLastname();
                sb.append(" ");
                sb.append(borrowerName);
            }
        }
        return borrowerName;
    }

    private BookDto createBookDto(String title, String authorName, String isbn, CategoryEnum category, LocalDate dateOfRelease, String borrower, Long bookId, String description, int pages) {
        BookDto bookDto = new BookDto();
        bookDto.setTitle(title);
        bookDto.setAuthorName(authorName);
        bookDto.setIsbn(isbn);
        bookDto.setCategory(category);
        bookDto.setDateOfRelease(dateOfRelease);
        bookDto.setBorrower(borrower);
        bookDto.setBookId(bookId);
        bookDto.setDescription(description);
        bookDto.setPages(pages);
        return bookDto;
    }
}
