package models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "author")
public class Author {
    @Id
    @Column(nullable = false, unique = true)
    private Long idAuthor;
    private String authorName;
    private String authorLastname;
    private String birthPlace;
    @OneToMany(mappedBy = "author")
    private List<Book>books;
}
