package models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "borrower")
public class Borrower {
    @Id
    @Column(nullable = false, unique = true)
    private Long idBorrower;
    @Column(nullable = false)
    private String borrowerName;
    @Column(nullable = false)
    private String borrowerLastname;
    @OneToOne
    @JoinColumn(name = "details")
    private BorrowerDetails detailsId;
    @OneToMany(mappedBy = "borrowerId")
    private List<Borrow> borrowsList;

}
