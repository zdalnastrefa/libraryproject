package models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "Borrow")
public class Borrow {
    @Id
    @Column(nullable = false)
    private Long idBorrow;
    @ManyToOne
    @JoinColumn(name = "borrowed_book_id")
    private Book bookId;
    @ManyToOne
    @JoinColumn(name = "borrowList")
    private Borrower borrowerId;
    @Column(nullable = false)
    private Date dateOfBorrow;

}
