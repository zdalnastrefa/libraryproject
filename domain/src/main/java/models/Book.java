package models;

import Enums.CategoryEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "Book")
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private Long bookId;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false, name = "category_book")
    private CategoryEnum category;
    @Column(nullable = false)
    private String isbn;
    @Column(nullable = false)
    private int pages;
    @Column(nullable = false)
    private LocalDate releaseDate;
    @Column(nullable = false)
    private String summary;
    @Column(nullable = false)
    private String title;
    @Column(nullable = false)
    private boolean isBorrow;
    @OneToMany(mappedBy = "bookId")
    private List<Borrow> borrows;
    @ManyToOne
    @JoinColumn(name = "author_name")
    private Author author;
}
