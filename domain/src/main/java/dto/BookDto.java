package dto;


import Enums.CategoryEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BookDto {
    private String title;
    private String authorName;
    private String isbn;
    private CategoryEnum category;
    private LocalDate dateOfRelease;
    private String borrower;
    private Long bookId;
    private String description;
    private int pages;
}
