package repository;

import models.Author;
import org.hibernate.Session;
import org.hibernate.Transaction;
import repository.util.HibernateUtil;

import java.util.ArrayList;
import java.util.List;

public class AuthorRepository extends CrudRepository<Author,Long> {
    public List<Author> findAll(){
        List<Author> authors = new ArrayList<>();
        Transaction tx = null;
        try (Session session = HibernateUtil.openSession()){
            tx = session.getTransaction();
            tx.begin();
            authors = session.createQuery("FROM Author").list();
            tx.commit();
        }catch (Exception e){
            if (tx != null){
                tx.rollback();
            }
        }
        return  authors;
    }
}
