package repository;

import models.Book;
import org.hibernate.Session;
import org.hibernate.Transaction;
import repository.util.HibernateUtil;

import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.List;

public class BookRepository extends CrudRepository<Book,Long> {
    public List<Book> findAll(){
        List<Book> books = new ArrayList<>();
        try (Session session = HibernateUtil.openSession()){
            books = session.createQuery("FROM Book", Book.class).list();
        return  books;
    }
}}
