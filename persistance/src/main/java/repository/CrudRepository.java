package repository;
import org.hibernate.Session;
import org.hibernate.Transaction;
import repository.util.HibernateUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.lang.reflect.ParameterizedType;

public abstract class CrudRepository <T,K> {
    protected final Class<T> entityClass;

    @SuppressWarnings("unchecked")
    protected CrudRepository() {
        ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
        this.entityClass = (Class<T>) genericSuperclass.getActualTypeArguments()[0];
    }

        public void create(T entity) {
        Transaction tx = null;
        try (Session session = HibernateUtil.openSession()) {
            tx = session.getTransaction();
            tx.begin();
            session.persist(entity);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
        }
    }
        public T read(K id) {
        Session session = HibernateUtil.openSession();
            return session.find(entityClass, id);
        }



    public void edit(T k){
        Transaction tx = null;
        try (Session session = HibernateUtil.openSession()) {
            tx = session.getTransaction();
            tx.begin();
            session.merge(k);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
        }
    }

    public void delete(T entity){
    Transaction tx = null;
    try (Session session = HibernateUtil.openSession()) {
        tx = session.getTransaction();
        tx.begin();
        session.remove(entity);
        tx.commit();
    } catch (Exception e) {
        if (tx != null) {
            tx.rollback();
        }
    }
}}
